<?php
session_start();
if (!(isset($_SESSION['logado']))) {
    echo "<script>alert('Sem Permissao')</script>";
    header("Refresh: 0;url = index.php");
}
include 'header.php';

include 'contador.php';

include 'conexao.php';

$crescente = $conn->query("SELECT * FROM produto ORDER BY preço ASC");

$decrescente = $conn->query("SELECT * FROM produto ORDER BY preço DESC");

?>


<title>Painel de Controle</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="#">Produtos</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="text-right">
                <a href="sair.php">Sair</a>
            </div>
        </div>
        </div>
    </nav>
    <div class="container">

        <div class="row">
            <div class="col text-center">
                <ul class="list-group">
                    <li class="list-group-item">
                        <h2>Crescente</h2>
                    </li>
                </ul>

                <table class="table table-dark">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Nome</th>
                            <th scope="col">Preço</th>
                            <th scope="col">Quantidade</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if ($crescente) {
                            foreach ($crescente as $row) {
                                echo "<tr><td>" . $row['nome'];
                                echo "<td>" . $row['preço'] . "<td>";
                                echo $row['quant'] . "</td></tr>";
                            }
                        }
                        ?>

                    </tbody>
                </table>

            </div>
            <div class="col text-center">
                <ul class="list-group">
                    <li class="list-group-item">
                    
                    <h4> <?php echo "Visitante número <strong>" . $contador . "</strong>"; ?></h4>
                    </li>
                </ul>
                
                <br><br>
                <form action="salvarproduto.php" method="post">
                    <ul class="list-group">
                        <li class="list-group-item"><input type="text" name="nome" placeholder="nome"></li>
                        <li class="list-group-item"> <input type="text" name="preço" placeholder="preço"></li>
                        <li class="list-group-item"><input type="text" name="quantidade" placeholder="quantidade"></li>
                    </ul>
                    
                    <LI class="list-group-item"><input type="submit" class="btn btn-dark btn-block" value="Cadastrar"></LI>
                
                </form>
            </div>
            <div class="col text-center">
                <ul class="list-group">
                    <li class="list-group-item">
                        <h2>Decrescente</h2>
                    </li>
                </ul>

                <table class="table table-dark">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Nome</th>
                            <th scope="col">Preço</th>
                            <th scope="col">Quantidade</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if ($decrescente) {
                            foreach ($decrescente as $row) {
                                echo "<tr><td>" . $row['nome'];
                                echo "<td>" . $row['preço'] . "<td>";
                                echo $row['quant'] . "</td></tr>";
                            }
                        }
                        ?>

                    </tbody>
                </table>

            </div>
        </div>
    </div>

</body>

</html>