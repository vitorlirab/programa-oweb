<?php
session_start();

if ((isset($_POST['login'])) && (isset($_POST['senha1']))) {
    $_SESSION['login'] = $_POST['login'];
    $_SESSION['senha'] = $_POST['senha1'];
} elseif (isset($_POST['senha1'])) {
    $_SESSION['senha'] = $_POST['senha1'];
}

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title>Login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>

<body>
    <div class="container-login100">
        <div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
            <form class="login100-form validate-form" method="POST" action="paineldecontrole.php">
                <span class="login100-form-title p-b-37">
                    Login
                </span>
                <div class="wrap-input100 validate-input m-b-20" data-validate="Usuario">
                    <input class="input100" type="text" name="login" placeholder="Usuario">
                </div>
                <div class="wrap-input100 validate-input m-b-25" data-validate="Digite a senha">
                    <input class="input100" type="password" name="senha" placeholder="Senha">
                </div>
                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        Entrar
                    </button>
                </div>
                <div class="text-center p-t-57 p-b-20">
                    <span class="txt1">
                        <p><a href="cadastro.php">Criar conta</a></p>
                        <p><a href="recuperarsenha.php">Esqueci minha senha</a></p>
                    </span>
                </div>
            </form>
        </div>
    </div>

    <div id="dropDownSelect1"></div>

    <!--===============================================================================================-->
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/bootstrap/js/popper.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/daterangepicker/moment.min.js"></script>
    <script src="vendor/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script src="js/main.js"></script>

</body>

</html>