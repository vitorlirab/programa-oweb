<?php

session_start();
if ((isset($_SESSION['login'])) && (isset($_SESSION['senha']))) {
    $usuario = $_SESSION['login'];
    $senha = $_SESSION['senha'];
}

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title>Recuperar Senha</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--===============================================================================================-->
</head>

<body>
    <div class="container-login100">
        <div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
            <form class="login100-form validate-form" method="POST" action="index.php" id="idDoForm">
                <span class="login100-form-title p-b-37">
                    Recuperar Senha
                </span>
                <div class="login100-form-subtitle p-b-37">
                    <h5 class="user"> Usuario: <?php if (isset($usuario)) {
                                                    echo "<strong>$usuario</strong>";
                                                }; ?></h5>
                    <h5> Senha: <?php if (isset($senha)) {
                                    echo "<strong>$senha</strong>";
                                }; ?></h5>
                </div>

                <div class="wrap-input100 validate-input m-b-20" data-validate="Entre com usuario">
                    <input class="input100" type="text" name="login" require disabled placeholder="<?php if (isset($usuario)) {
                                                    echo "$usuario";
                                                }; ?>">
                </div>
                <div class="wrap-input100 validate-input m-b-20" data-validate="Senha">
                    <input class="input100" type="password" name="senha1" id="senha1" placeholder="Senha">

                </div>
                <div class="wrap-input100 validate-input m-b-20" data-validate="Confirme a senha">
                    <input class="input100" type="password" name="senha2" id="senha2" placeholder="Confirme a senha">

                </div>
                <div class="container-login100-form-btn">
                    <button class="login100-form-btn" onclick="validarSenha();">
                        Mudar
                    </button>
                </div>
                <div class="text-center p-t-57 p-b-20">
                    <span class="txt1">
                        <a href="index.php">Voltar</a>
                    </span>
                </div>
            </form>
        </div>
    </div>
    <div id="dropDownSelect1"></div>

    <!--===============================================================================================-->
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/bootstrap/js/popper.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/daterangepicker/moment.min.js"></script>
    <script src="vendor/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script src="js/main.js"></script>
    <script>
        function validarSenha() {
            if ((document.getElementById('senha2').value) != (document.getElementById('senha1').value)) {
                alert('Senhas diferentes por favor corrija!')
                formulario = document.getElementById("idDoForm")
                formulario.onsubmit = function() {
                    return false;
                }
            } else {
                formulario = document.getElementById("idDoForm")
                formulario.onsubmit = function() {
                    return true;
                }
                alert('Dados atualizados com sucesso!')
            }
        }
    </script>
</body>

</html>