<?php
session_start();
if (!(isset($_SESSION['logado']))) {
    echo "<script>alert('Sem Permissao')</script>";
    header("Refresh: 0;url = index.php");
}
include 'header.php';

include 'conexao.php';

$crescente = $conn->query("SELECT * FROM produto ORDER BY preço ASC");

$decrescente = $conn->query("SELECT * FROM produto ORDER BY preço DESC");

?>


<title>Painel de Controle</title>
</head>

<body>
    <h2 id="title">Produto</h2>
    <div class="main">
        <div class="adesivo1">
            <h2>Crescente</h2>
            <hr>
            <?php
            if ($crescente) {
                foreach ($crescente as $row) {
                    echo $row['nome'] . ' R$ ' . $row['preço'] . '  ' . $row['quant'] . '</br>';
                }
            }

            ?>
        </div>
        </form>
        <div class="adesivo2">
            <form action="salvarproduto.php" method="post">
                <input type="text" name="nome" placeholder="nome">
                <input type="text" name="preço" placeholder="preço">
                <input type="text" name="quantidade" placeholder="quantidade">
                <input type="submit" value="Cadastrar">
            </form>
        </div>
        <div class="adesivo3">
            <h2>Decrescente</h2>
            <hr>
            <?php
            if ($decrescente) {
                foreach ($decrescente as $row) {
                    echo $row['nome'] . ' R$ ' . $row['preço'] . '  ' . $row['quant'] . '</br>';
                }
            }

            ?>
        </div>
    </div>

</body>

</html>