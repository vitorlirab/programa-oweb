<?php
$data = date($_POST["nasc"]);
$data = explode("-", $data);
list($ano, $mes, $dia) = $data;
$anoNasc = intval($ano);
$mesNasc = intval($mes);
$diaNasc = intval($dia);

$diaHoje = date("d");
$mesHoje = date("m");
$anoHoje = date("Y");

$usuario = $_POST["usuario"];
$email = $_POST["email1"];

if ($diaNasc >= $diaHoje && $mesNasc >= $mesHoje) {
    $idade = $anoHoje - $anoNasc - 1;
} elseif ($mesNasc >= $mesHoje) {
    $idade = $anoHoje - $anoNasc - 1;
} else {
    $idade = $anoHoje - $anoNasc;
}
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title>Sucesso!</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--===============================================================================================-->
</head>

<body>
    <div class="container-contact100">
        <div class="wrap-contact100">
            <span class="contact100-form-title">
                Cadastro efetuado com sucesso !
            </span>
            <div class="wrap-input100 validate-input">
                <input value="Seu usuario">
                <span class="input100"><?php echo $usuario; ?></span>
            </div>
            <div class="wrap-input100 validate-input">
                <input value="Seu Email">
                <span class="input100"><?php echo $email; ?></span>
            </div>
            <div class="wrap-input100 validate-input">
                <input value="Sua Idade">
                <span class="input100"><?php echo $idade; ?></span>
            </div>
        </div>
    </div>
</body>

</html>